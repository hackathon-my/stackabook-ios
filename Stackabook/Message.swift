//
//  Message.swift
//  Stackabook
//
//  Created by Muaz on 6/14/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class Message: NSObject {
    var displayName: String
    var text: String
    var userId: Int
    
    init(displayName: String, text: String, userId: Int) {
        self.displayName = displayName
        self.text = text
        self.userId = userId
    }
}
