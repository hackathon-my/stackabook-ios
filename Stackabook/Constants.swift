//
//  Constants.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

struct Theme {
    static let gold = UIColor(hex: "#F9A825")
    static let black = UIColor(hex: "#000000")
    static let textGray = UIColor(hex: "#999999")
    static let lightGray = UIColor(hex: "#BBBBBB")
    static let white = UIColor.whiteColor()
    static let fontTableList = UIFont(name: "GentiumBookBasic", size: 17)
}

struct API {
    static let recentBooks = "http://api.stackabook.com/v1/recentbooks"
    static let placeSuggestion = "http://api.stackabook.com/v1/placesuggest?q="
    static let bookList = "http://api.stackabook.com/v1/books/search?q="
}

// Font Names = [[GentiumBookBasic, GentiumBookBasic-BoldItalic, GentiumBookBasic-Italic, GentiumBookBasic-Bold]]
// Font Names = [[SanFranciscoText-Regular, SanFranciscoText-Medium, SanFranciscoText-Bold]]