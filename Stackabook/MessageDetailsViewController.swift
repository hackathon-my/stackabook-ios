//
//  MessageDetailsViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/14/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class MessageDetailsViewController: SLKTextViewController {
    
    var messageArray = [String]()
    
    override class func tableViewStyleForCoder(decoder: NSCoder) -> UITableViewStyle {
        return UITableViewStyle.Plain;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Message"
        self.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "MessageCell")
        
        messageArray = ["Ok", "asdasd", "besssssss"]
        
        // Slack settings
        self.inverted = true
    }

    // MARK: - UITableView 
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("MessageCell") as! UITableViewCell
        self.configureMessageCell(cell, indexPath: indexPath)
        cell.transform = self.tableView.transform;
        return cell
    }
    
    func configureMessageCell(cell: UITableViewCell, indexPath: NSIndexPath) {
//        let bookImage = cell.contentView.viewWithTag(1) as! UIImageView
//        bookImage.layer.cornerRadius = bookImage.frame.size.width / 2
//        let title = cell.contentView.viewWithTag(2) as! UILabel
//        let desc = cell.contentView.viewWithTag(3) as! UILabel
        cell.textLabel?.text = self.messageArray[indexPath.row]
    }
    
    
    // MARK: - Slack stuff
    
    override func didPressRightButton(sender: AnyObject!) {
        self.textView.refreshFirstResponder()
        
        println("Ok \(self.textView.text)")
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let rowAnimation = self.inverted ? UITableViewRowAnimation.Bottom : UITableViewRowAnimation.Top
        let scrollPosition = self.inverted ? UITableViewScrollPosition.Bottom : UITableViewScrollPosition.Top
        
        self.tableView.beginUpdates()
        self.messageArray.insert(self.textView.text, atIndex: 0)
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: rowAnimation)
        self.tableView.endUpdates()
        
        self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: scrollPosition, animated: true)
        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        
        
        super.didPressRightButton(sender)
    }
    
}
