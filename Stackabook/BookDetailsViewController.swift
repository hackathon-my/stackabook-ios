//
//  BookDetailsViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit
import SwiftyJSON

class BookDetailsViewController: UITableViewController {
    
    var dict: [String: JSON]!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 200.0;
        
        println("DDDDD \(dict)")
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        self.configureCell(cell, indexPath: indexPath)
        return cell
    }

    func configureCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        let bookImage = cell.contentView.viewWithTag(1) as! UIImageView
        let imgUrl = dict["cover"]!.stringValue
        bookImage.sd_setImageWithURL(NSURL(string: "http://api.stackabook.com\(imgUrl)"), placeholderImage: UIImage(named: "placeholder-book")!)
        bookImage.userInteractionEnabled = true
        let ges = UITapGestureRecognizer(target: self, action: "tapProfileImage:")
        bookImage.addGestureRecognizer(ges)
        
        let title = cell.contentView.viewWithTag(2) as! UILabel
        title.text =  dict["name"]!.stringValue
        
        let author = cell.contentView.viewWithTag(3) as! UILabel
        author.text = dict["author"]!.stringValue

        let desc = cell.contentView.viewWithTag(4) as! UILabel
        desc.text = dict["desc"]!.stringValue
    }
    
    func tapProfileImage(sender: UIImageView) {
        println("Tapped \(sender)")
        let imageInfo = JTSImageInfo()
        
        let imgUrl = dict["cover"]!.stringValue
        imageInfo.imageURL = NSURL(string: "http://api.stackabook.com\(imgUrl)")
        imageInfo.referenceRect = sender.frame
        imageInfo.referenceView = sender.superview
        imageInfo.referenceContentMode = sender.contentMode
        imageInfo.referenceCornerRadius = sender.layer.cornerRadius
        
        let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle: JTSImageViewControllerBackgroundOptions.Blurred)
        imageViewer.showFromViewController(self, transition: JTSImageViewControllerTransition._FromOriginalPosition)
    }

}
