//
//  ProfieViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class ProfieViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNSegmentedControlDelegate {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDescription: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    let slideHeaderControl = DZNSegmentedControl(items: ["About", "Listing"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Profile"
        self.automaticallyAdjustsScrollViewInsets = false;
        self.setupProfileView()
        
        // Setup slide header
        
        slideHeaderControl.delegate = self
        slideHeaderControl.selectedSegmentIndex = 0
        slideHeaderControl.bouncySelectionIndicator = true
        slideHeaderControl.height = 44.0
        slideHeaderControl.addTarget(self, action: "didChangeSegment:", forControlEvents: UIControlEvents.ValueChanged)
        slideHeaderControl.showsCount = false
        slideHeaderControl.tintColor = Theme.gold
        
        self.tableView.tableHeaderView = self.slideHeaderControl
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
    }
    
    func setupProfileView() {
        self.profileImage.layer.cornerRadius = self.profileImage.frame.height/2
        self.userName.text = "Jack Wharton"
        self.userDescription.text = "Texas, USA"
        
        let tapGest = UITapGestureRecognizer(target: self, action: "tapProfileImage")
        self.profileImage.addGestureRecognizer(tapGest)
        
    }
    
    func tapProfileImage() {
        let imageInfo = JTSImageInfo()
        imageInfo.image = UIImage(named: "user0")
        imageInfo.referenceRect = self.profileImage.frame
        imageInfo.referenceView = self.profileImage.superview
        imageInfo.referenceContentMode = self.profileImage.contentMode
        imageInfo.referenceCornerRadius = self.profileImage.layer.cornerRadius
        
        let imageViewer = JTSImageViewController(imageInfo: imageInfo, mode: JTSImageViewControllerMode.Image, backgroundStyle: JTSImageViewControllerBackgroundOptions.Blurred)
        imageViewer.showFromViewController(self, transition: JTSImageViewControllerTransition._FromOriginalPosition)
    }
    
    func didChangeSegment(control: DZNSegmentedControl) {
        println("Changed \(control.selectedSegmentIndex)")
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if self.slideHeaderControl.selectedSegmentIndex == 0 {
            return 60
        } else {
            return 60
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.slideHeaderControl.selectedSegmentIndex == 0 {
            let cell = self.tableView.dequeueReusableCellWithIdentifier("AboutCell") as! UITableViewCell
            return cell
        } else {
            let cell = self.tableView.dequeueReusableCellWithIdentifier("ListingCell") as! UITableViewCell
            return cell
        }

    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.slideHeaderControl.selectedSegmentIndex == 0 ? 1 : 10
    }

    // MARK: - UIBarPositioning
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.Bottom
    }
}
