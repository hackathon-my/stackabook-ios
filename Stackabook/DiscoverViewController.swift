//
//  DiscoverViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let collectionCell = "CollectionSuggestCell"

class DiscoverViewController: UITableViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var loadingView: UIView!
    var recentBookList = [JSON]()
    var genreText = ["cookbooks","architecture","science","religion","music"]

    enum Sections: Int {
        case TopGenres, BookCategory
    }
    
    // MARK :- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Discover"
        self.tableView.backgroundColor = UIColor.whiteColor()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        // UICollectionView
        self.collectionView.backgroundColor = UIColor.clearColor()
        
        self.fetchRecentData()
    }
    
    func setRecentBooksLoadingView() {
        var frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width, height: self.collectionView.frame.height))
        self.loadingView = UIView(frame: frame)
        self.loadingView.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.8)
        let spinner = UIActivityIndicatorView(frame: frame)
        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        self.loadingView.addSubview(spinner)
        spinner.startAnimating()
        self.collectionView.addSubview(loadingView)
    }
    
    func removeRecentBooksLoadingView() {
        self.loadingView.removeFromSuperview()
    }
    
    func fetchRecentData() {
        self.setRecentBooksLoadingView()
        let urlString = API.recentBooks
        Alamofire.request(.GET, urlString).responseJSON() {
            (_, response, data, error) in
            if error == nil && response != nil && data != nil {
                
                let json = JSON(data!)
                println("JSON \(json)")
                self.recentBookList = json.arrayValue
                self.collectionView.reloadData()
                self.removeRecentBooksLoadingView()
            } else {
                println(error)
            }
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    
    // MARK :- UICollectionView
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recentBookList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionCell", forIndexPath: indexPath) as! UICollectionViewCell
        self.configureCollectionView(cell, indexPath: indexPath)
        return cell
    }
    
    func configureCollectionView(cell: UICollectionViewCell, indexPath: NSIndexPath) {
        
        let dict = self.recentBookList[indexPath.row].dictionaryValue
        let bookImage = cell.contentView.viewWithTag(1) as! UIImageView
        let bookImgUrl = dict["cover"]!.stringValue
        bookImage.sd_setImageWithURL(NSURL(string: bookImgUrl), placeholderImage: UIImage(named: "placeholder-book"))
        
        let title = cell.contentView.viewWithTag(2) as! UILabel
        title.text = dict["title"]!.stringValue
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        println("Select collection \(indexPath.row)")
    }
    
    // MARK :- UITableView
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel.font = UIFont(name: "SanFranciscoText-Bold", size: 13)
        headerView.textLabel.textColor = Theme.lightGray
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch Sections(rawValue: indexPath.section)! {
        case .TopGenres:
            return 100
        case .BookCategory:
            return tableView.rowHeight
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section == 0) ? 30 : 40
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        if self.tableView(tableView, numberOfRowsInSection: section) == 0 {
            return ""
        }
        
        switch Sections(rawValue: section)! {
        case .TopGenres:
            return "Top Genres"
        case .BookCategory:
            return "Category"
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView:
        UITableView, numberOfRowsInSection section: Int) -> Int {
            switch Sections(rawValue: section)! {
            case .TopGenres:
                return self.genreText.count
            case .BookCategory:
                return 4
            }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch Sections(rawValue: indexPath.section)! {
        case .TopGenres:
            let cell = self.tableView.dequeueReusableCellWithIdentifier("GenreCell") as! UITableViewCell
            cell.separatorInset = UIEdgeInsetsZero
            
            let tjk = self.genreText[indexPath.row]
            
            let genreImage = cell.contentView.viewWithTag(1) as! UIImageView
            genreImage.image = UIImage(named: "dis-\(tjk)")
            genreImage.layer.cornerRadius = 6
            
            let genreTitle = cell.contentView.viewWithTag(3) as! UILabel
            genreTitle.text = tjk.capitalizeFirst
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
        case .BookCategory:
            let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
            cell.textLabel?.text = "Book \(indexPath.row)"
            cell.textLabel?.font = Theme.fontTableList!
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
        }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("Tapped \(indexPath.row)")
    }
    
}
