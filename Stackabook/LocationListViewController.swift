//
//  LocationListViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit
import Alamofire

protocol LocationListViewControllerDelegate {
    func didFinishPickLocation(locString: String)
}

class LocationListViewController: UITableViewController, UISearchBarDelegate {
    
    var searchBar: UISearchBar!
    var suggestedList = [String]()
    var delegate: LocationListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "SuggestionsCell")
        
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        searchBar = UISearchBar(frame: CGRectMake(0, 0, self.view.frame.size.width-54, 44))
        searchBar.translucent = false
        searchBar.searchBarStyle = UISearchBarStyle.Minimal
        searchBar.backgroundImage = UIImage()
        searchBar.tintColor = Theme.gold
        searchBar.barTintColor = UIColor.whiteColor()
        searchBar.placeholder = "Select location"
        searchBar.delegate = self
        searchBar.returnKeyType = .Search
        
        let closeButton = UIButton.buttonWithType(UIButtonType.System) as! UIButton
        closeButton.frame = CGRectMake(self.view.frame.size.width-54, 0, 44, 44)
        closeButton.setImage(UIImage(named: "navbar-cancel"), forState: UIControlState.Normal)
        closeButton.tintColor = Theme.gold
        closeButton.addTarget(self, action: "closeCurrentVC", forControlEvents: UIControlEvents.TouchUpInside)
        
        let titleView = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 44))
        titleView.addSubview(searchBar)
        titleView.addSubview(closeButton)
        
        self.navigationItem.titleView = titleView
    }
    
    // MARK: - Helper
    
    func closeCurrentVC() {
        self.searchBar.resignFirstResponder()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func fetchKeywordSuggestion(key: String) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let keyword = key.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        
        let urlString = "\(API.placeSuggestion)\(keyword!)"
        println("UrlString \(urlString)")
        
        request(.GET, urlString).responseJSON() {
            (_, response, data, error) in
            if error == nil && response != nil && data != nil {
                
                self.suggestedList = data as! Array<String>
                self.tableView.reloadData()
                
            } else {
                println(error)
            }
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    
    // MARK: SearchBar / ScrollView Delegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        let keyword = searchText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let keywordLength = count(keyword)
        if keywordLength > 1 {
            println("Input string \(keyword)")
            self.fetchKeywordSuggestion(keyword)
        } else if keywordLength == 0 {
            println("Search bar cleared")
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {

            var keyword = searchBar.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            if  count(keyword) > 1 {
                // Run delegate
            }

        self.searchBar.resignFirstResponder()
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.suggestedList.count
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel.font = UIFont(name: "SanFranciscoText-Bold", size: 13)
        headerView.textLabel.textColor = Theme.lightGray
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.suggestedList.count > 0 ? "Locations" : nil
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier("SuggestionsCell", forIndexPath: indexPath) as! UITableViewCell
            cell.textLabel?.text = self.suggestedList[indexPath.row]
            cell.textLabel?.font = UIFont(name: "SanFranciscoText-Regular", size: 15)
            return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.searchBar.resignFirstResponder()
        self.closeCurrentVC()
        
        if let delegate = self.delegate {
            delegate.didFinishPickLocation(self.suggestedList[indexPath.row])
        }
    }
}
