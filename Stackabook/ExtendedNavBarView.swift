//
//  ExtendedNavBarView.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class ExtendedNavBarView: UIView {
    
    override func willMoveToWindow(newWindow: UIWindow?) {
        self.layer.shadowOffset = CGSizeMake(0, 1.0 / UIScreen.mainScreen().scale)
        self.layer.shadowRadius = 0
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowOpacity = 0.25
    }

    override func drawRect(rect: CGRect) {
    }

}
