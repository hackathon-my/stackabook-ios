//
//  SuggestionsResultsController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class SuggestionsResultsController: UITableViewController {
    
    var suggestionList = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
    }
    
    // MARK: - UITableView

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestionList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = suggestionList[indexPath.row]
        return cell
    }


}
