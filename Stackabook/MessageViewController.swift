//
//  MesssageViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class MessageViewController: UITableViewController {
    
    var nameList = ["Jon James", "Kamal Zainal", "Therry White", "Carrie Junior", "Dave Band", "Quan Yuuu", "Thom Light", "Rainbow Surfer", "James", "Jennifer Vim", "Her Rey"]
        var textList = ["Ok thanks for the info", "Will meet at 12pm.", "😄😄😄😄😄😄😄😄😄😄😄😄😄", "Good books, thank you very much.", "I think the book you gave me.. ", "Alright!!!", "Ok", "Jump", "T", "Bye", "I want more books"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Message"
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)

    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("MessageCell") as! UITableViewCell
        self.configureMessageCell(cell, indexPath: indexPath)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("Tapped \(indexPath.row)")
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let messageDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier("MessageDetailsView") as! MessageDetailsViewController
//        let navController = UINavigationController(rootViewController: messageDetailsVC)
//        self.navigationController?.presentViewController(navController, animated: true, completion: nil)
        self.navigationController?.pushViewController(messageDetailsVC, animated: true)
        
    }
    
    func configureMessageCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        let bookImage = cell.contentView.viewWithTag(1) as! UIImageView
        bookImage.layer.cornerRadius = bookImage.frame.size.width / 2
        bookImage.image = UIImage(named: "128-\(indexPath.row).jpg")
        let title = cell.contentView.viewWithTag(2) as! UILabel
        title.text = nameList[indexPath.row]
        let desc = cell.contentView.viewWithTag(3) as! UILabel
        desc.text = textList[indexPath.row]
    }
    
}
