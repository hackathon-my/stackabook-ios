//
//  AddBookViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit
import AVFoundation

class AddBookViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var instructLabel: UILabel!
    @IBOutlet weak var scannerView: UIView!
    let session         : AVCaptureSession = AVCaptureSession()
    var previewLayer    : AVCaptureVideoPreviewLayer!
    var highlightView   : UIView = UIView()
    lazy var device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    
    @IBOutlet weak var lightBtn: UIButton!
    @IBOutlet weak var submitFormBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lightBtn.addTarget(self, action: "switchLight:", forControlEvents: UIControlEvents.TouchUpInside)
        self.lightBtn.tintColor = UIColor(hex: "#FF8000")
        
//        self.submitFormBtn.addTarget(self, action: "openSubmitViewController", forControlEvents: UIControlEvents.TouchUpInside)
        self.submitFormBtn.setBackgroundImage(UIImage(color: Theme.black), forState: UIControlState.Normal)
        self.submitFormBtn.layer.cornerRadius = 6
        self.view.backgroundColor = UIColor.blackColor()
        
        self.highlightView.autoresizingMask =   UIViewAutoresizing.FlexibleTopMargin |
            UIViewAutoresizing.FlexibleBottomMargin |
            UIViewAutoresizing.FlexibleLeftMargin |
            UIViewAutoresizing.FlexibleRightMargin
        
        self.highlightView.layer.borderColor = UIColor.greenColor().CGColor
        self.highlightView.layer.borderWidth = 3
        
        self.scannerView.addSubview(self.highlightView)
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        var error : NSError? = nil
        
        let input : AVCaptureDeviceInput? = AVCaptureDeviceInput.deviceInputWithDevice(device, error: &error) as? AVCaptureDeviceInput
        
        if input != nil {
            session.addInput(input)
        } else {
            println(error)
        }
        
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        session.addOutput(output)
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        
        previewLayer = AVCaptureVideoPreviewLayer.layerWithSession(session) as! AVCaptureVideoPreviewLayer
        previewLayer.frame = self.scannerView.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.scannerView.layer.addSublayer(previewLayer)
        session.startRunning()
        
        self.scannerView.bringSubviewToFront(self.lightBtn)
        self.scannerView.bringSubviewToFront(self.instructLabel)
        self.scannerView.bringSubviewToFront(self.submitFormBtn)

    }
    
    func hasTorch() -> Bool {
        if let d = self.device {
            return d.hasTorch
        }
        return false
    }
    
    func switchLight(sender: UIButton) {
        sender.selected = !sender.selected
        
        self.session.beginConfiguration()
        self.toggleTorch()
    }
    
    func toggleTorch() -> Bool {
        if self.hasTorch() {
            self.session.beginConfiguration()
            self.device.lockForConfiguration(nil)
            
            if self.device.torchMode == .Off {
                self.device.torchMode = .On
            } else if self.device.torchMode == .On {
                self.device.torchMode = .Off
            }
            
            self.device.unlockForConfiguration()
            self.session.commitConfiguration()
            
            return self.device.torchMode == .On
        }
        return false
    }
    
    override func viewDidAppear(animated: Bool) {
        session.startRunning()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        session.stopRunning()
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        var highlightViewRect = CGRectZero
        var barCodeObject : AVMetadataObject!
        var detectionString : String!
        
        let barCodeTypes = [AVMetadataObjectTypeUPCECode,
            AVMetadataObjectTypeCode39Code,
            AVMetadataObjectTypeCode39Mod43Code,
            AVMetadataObjectTypeEAN13Code,
            AVMetadataObjectTypeEAN8Code,
            AVMetadataObjectTypeCode93Code,
            AVMetadataObjectTypeCode128Code,
            AVMetadataObjectTypePDF417Code,
            AVMetadataObjectTypeQRCode,
            AVMetadataObjectTypeAztecCode
        ]
        
        for metadata in metadataObjects {
            
            for barcodeType in barCodeTypes {
                
                if metadata.type == barcodeType {
                    barCodeObject = self.previewLayer.transformedMetadataObjectForMetadataObject(metadata as! AVMetadataMachineReadableCodeObject)
                    
                    highlightViewRect = barCodeObject.bounds
                    
                    detectionString = (metadata as! AVMetadataMachineReadableCodeObject).stringValue
                    self.checkCodeValidity(detectionString)
                    self.session.stopRunning()
                    break
                }
                
            }
        }
        
        self.highlightView.frame = highlightViewRect
        self.scannerView.bringSubviewToFront(self.highlightView)
        
    }
    
    func checkCodeValidity(code: String) {
        let alert = UIAlertView(title: "Success", message: "Code \(code)", delegate: nil, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func openSubmitViewController() {
        let rentFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("RentFormView") as! RentFormViewController
        let navCont = UINavigationController(rootViewController: rentFormVC)
        self.presentViewController(navCont, animated: true) { () -> Void in }

    }

}
