//
//  SearchViewController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, CLLocationManagerDelegate, LocationListViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectLocationBtn: UIButton!
    
    var searchController:UISearchController!
    let suggestionsResultsController = SuggestionsResultsController()
    
    let locationManager = CLLocationManager()
    var userLocation = CLLocation()
    
    var shopResultList = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage(named: "TransparentPixel")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Pixel"), forBarMetrics: UIBarMetrics.Default)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = Theme.gold
        self.setLocationLabelTitle("Select location")
        
        // Search controller
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.sizeToFit()
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.searchBarStyle = .Minimal
        self.searchController.searchBar.placeholder = "Find books to rent"
        self.searchController.searchBar.delegate = self
        
        self.navigationItem.titleView = self.searchController.searchBar
        definesPresentationContext = true
        
        // Selection
        self.selectLocationBtn.setTitle("Select location   ▾", forState: UIControlState.Normal)
        self.selectLocationBtn.addTarget(self, action: "pushSelectLocation:", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.startLocationUpdates()
        self.setupTableBackground()
    }
    
    func setupTableBackground() {
        let bgImage = UIImageView(frame: CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height))
        bgImage.image = UIImage(named: "empty-table")
        bgImage.contentMode = UIViewContentMode.Center
        self.tableView.backgroundView = bgImage
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
    }
    
    func clearTableBackground() {
        self.tableView.backgroundView = nil
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
    }
    
    func fetchShopFromKeyword(key: String) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let keyword = key.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        
        let urlString = "\(API.bookList)\(keyword!)"
        println("UrlString \(urlString)")
        
        Alamofire.request(.GET, urlString).responseJSON() {
            (_, response, data, error) in
            if error == nil && response != nil && data != nil {
                
                let json = JSON(data!)
                let dict = json["items"]
                println("JSON \(json)")
                
                self.shopResultList = dict.arrayValue
                self.tableView.reloadData()
                
                if self.shopResultList.isEmpty {
                    self.setupTableBackground()
                } else {
                    self.clearTableBackground()
                }
                
            } else {
                println(error)
            }
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    
    
    func fetchSuggestionFromKeyword(key: String) {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            let keyword = key.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
            
            let urlString = "\(API.bookList)\(keyword!)"
            println("UrlString \(urlString)")
            
            Alamofire.request(.GET, urlString).responseJSON() {
                (_, response, data, error) in
                if error == nil && response != nil && data != nil {
                    
                    let json = JSON(data!)
                    let dict = json["items"]
                    println("JSON \(json)")
                    
                    self.shopResultList = dict.arrayValue
                    
                } else {
                    println(error)
                }
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }

    }
    
    // MARK: SearchBar / ScrollView Delegate
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        let keyword = searchText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let keywordLength = count(keyword)
        if keywordLength > 1 {
            println("Input string \(keyword)")
            // Fetch suggested
        } else if keywordLength == 0 {
            println("Search bar cleared")
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        println("Siap")
        var keyword = searchBar.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if  count(keyword) > 0 {
            searchController.active = false
            searchController.searchBar.text = keyword
            self.fetchShopFromKeyword(keyword)
        }
    }
    
    // MARK: - List delegate
    
    func didFinishPickLocation(locString: String) {
        println("Delegate \(locString)")
        self.setLocationLabelTitle(locString)
        
        let currentString = self.searchController.searchBar.text
        let finalString = "\(currentString) \(locString)"
        fetchShopFromKeyword(finalString)
        self.searchController.searchBar.text = finalString
    }
    
    func setLocationLabelTitle(title: String){
        self.selectLocationBtn.setTitle("\(title)   ▾", forState: UIControlState.Normal)
    }
    
    // MARK: - Location
    
    func startLocationUpdates() {
        
        println("Start location update")
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        if self.locationManager.respondsToSelector(Selector("requestAlwaysAuthorization")) {
            self.locationManager.requestAlwaysAuthorization()
        }
        self.locationManager.startUpdatingLocation()
    }
    
    // Location manager delegate
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        self.userLocation = locations.last as! CLLocation
        println("Current location: \(self.userLocation)")
        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: - Search

    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let searchText = searchController.searchBar.text
        println("Search \(searchText)")
    }

    // MARK :- Navigation
    
    func pushSelectLocation(sender: AnyObject) {
        let locationListVC = LocationListViewController()
        locationListVC.delegate = self
        let navController = UINavigationController(rootViewController: locationListVC)
        self.navigationController?.presentViewController(navController, animated: true, completion: { () -> Void in
            locationListVC.searchBar.becomeFirstResponder()
        })
    }
    
    // MARK :- UITableView
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopResultList.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("ResultCell") as! UITableViewCell
        self.configureResultCell(cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("Tapped \(indexPath.row)")
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let dict = self.shopResultList[indexPath.row].dictionaryValue
        
        let bookDetailsVC = self.storyboard?.instantiateViewControllerWithIdentifier("BookDetailsView") as! BookDetailsViewController
        bookDetailsVC.dict = dict
        self.navigationController?.pushViewController(bookDetailsVC, animated: true)
        
    }
    
    func configureResultCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        
        let dict = self.shopResultList[indexPath.row].dictionaryValue
        
        let bookImage = cell.contentView.viewWithTag(1) as! UIImageView
        let imgUrl = dict["cover"]!.stringValue
        bookImage.sd_setImageWithURL(NSURL(string: "http://api.stackabook.com\(imgUrl)"), placeholderImage: UIImage(named: "placeholder-book")!)
        let u = "http://api.stackabook.com\(imgUrl)"
        println("Link \(u)")
        let title = cell.contentView.viewWithTag(2) as! UILabel
        
        let name = dict["name"]!.stringValue
        let author = dict["author"]!.stringValue
        let completeString = "\(name)\nby \(author)"
        let attText = NSMutableAttributedString(string: completeString)
        
        
        let range = NSMakeRange(completeString.length - (author.length + 3), author.length + 3)
        attText.addAttribute(NSForegroundColorAttributeName, value: UIColor.grayColor(), range: range)
        attText.addAttribute(NSFontAttributeName, value: UIFont(name: "GentiumBookBasic", size: 15)!, range: range)
        title.attributedText = attText
        
        let desc = cell.contentView.viewWithTag(3) as! UILabel
        let d = dict["category"]!.stringValue
        desc.text = d
        
    }
    


}
