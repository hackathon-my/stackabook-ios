//
//  CustomTabBarController.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        for v in self.viewControllers as! [UIViewController] {
            v.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
    }

}
