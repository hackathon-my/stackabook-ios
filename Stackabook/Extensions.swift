//
//  Extensions.swift
//  Stackabook
//
//  Created by Muaz on 6/13/15.
//  Copyright (c) 2015 Kizuna. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hex: String) {
        var hexString = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (hexString.hasPrefix("#")) {
            hexString = hexString.substringFromIndex(advance(hexString.startIndex, 1))
        }
        
        if (count(hexString) != 6) {
            self.init(red:255.0, green:255.0, blue:255.0, alpha:1)
            return
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: hexString).scanHexInt(&rgbValue)
        let color = (
            red: CGFloat((rgbValue >> 16) & 0xff) / 255,
            green: CGFloat((rgbValue >> 08) & 0xff) / 255,
            blue: CGFloat((rgbValue >> 00) & 0xff) / 255
        )
        self.init(red:color.red, green:color.green, blue:color.blue, alpha:1)
    }
    
}

extension UIImage {
    
    func color(color: UIColor) -> UIImage {
        
        let rect: CGRect = CGRectMake(0, 0, 1, 1)
        UIGraphicsBeginImageContext(rect.size)
        let context: CGContextRef = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    convenience init?(color: UIColor) {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        self.init(CGImage:UIGraphicsGetImageFromCurrentImageContext().CGImage)
        UIGraphicsEndImageContext()
    }
}

extension CAGradientLayer {
    
    func darkGradient() -> CAGradientLayer {
        
        let topColor = UIColor.clearColor()
        let bottomColor = UIColor(white: 0.0, alpha: 0.5)
        
        let gradientColors: Array <AnyObject> = [topColor.CGColor, bottomColor.CGColor]
        let gradientLocations: Array <AnyObject> = [0.0, 1.0]
        
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.locations = gradientLocations
        
        return gradientLayer
    }
}

extension String {
    var length: Int {
        return count(self)
    }
    
    var capitalizeFirst:String {
        var result = self
        result.replaceRange(startIndex...startIndex, with: String(self[startIndex]).capitalizedString)
        return result
    }
    
    var capitalizeWord:String {
        var result = Array(self)
        if !isEmpty { result[0] = Character(String(result.first!).uppercaseString) }
        return String(result)
    }
}

